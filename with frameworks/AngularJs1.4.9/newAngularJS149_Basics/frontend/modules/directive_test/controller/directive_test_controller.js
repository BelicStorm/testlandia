app.controller("directive_test_controller", directive_test_controller);

function directive_test_controller() {
    var directive_test_controller = this;

    directive_test_controller.items = ["A", "B", "C", "D"];
    directive_test_controller.selected1 = "";
    directive_test_controller.selected2 = "";
     directive_test_controller.select = function(item) {
        if (!directive_test_controller.selected1) {
            directive_test_controller.selected1 = item;
            return;
        }
        
        // second select
        if (!directive_test_controller.selected2) {
            if (directive_test_controller.selected1 == item) {
                directive_test_controller.selected1 = "";
                return;
            }else {
                directive_test_controller.selected2 = item;
                directive_test_controller.compare = {item1:directive_test_controller.selected1,item2:directive_test_controller.selected2};
                return;
            }
        }
         // back to first select again
        directive_test_controller.selected1 = item;
        directive_test_controller.selected2 = "";
        directive_test_controller.compare = {item1:"",item2:""};
     }

}