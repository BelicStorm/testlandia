app.directive("test",["dialog","$compile","$rootScope",function(dialog,$compile,$rootScope){
    var controller = function () {
        var vm = this;
        console.log($rootScope.compare_items);
        if($rootScope.compare_items){
            vm.producto1=$rootScope.compare_items.item1;
            vm.producto2=$rootScope.compare_items.item2;
        }
        
        vm.addItem = function () {
            $rootScope.compare_items=vm.id;
            dialog.showAdvanced('<test type="details" id="vm.id"></test>');
        };
      };
      var template_button = '<button ng-click="vm.addItem()">Compare</button>';
      var template_details= '<span>{{vm.producto1}} y el {{vm.producto2}}</span>';
      function template(type){
            var pre_template="";
            switch (type) {
                case "button":
                    pre_template =  template_button;
                    break;
                case "details":
                    pre_template=  template_details;
                    break; 
                }
            return pre_template;
        };
        var linker = function(scope, element, attrs) {
            console.log(scope.vm);
            element.html(template(scope.vm.type)).show;
            $compile(element.contents())(scope);
        }   
      return {
          restrict: 'E', //Default for 1.3+
          scope: {
              id: '=id',
              type: '@',
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          /* template: template */
          link: linker
      };


}])