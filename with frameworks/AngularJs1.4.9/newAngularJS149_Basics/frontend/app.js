var app = angular.module('angularBasics', ["ngRoute",'ngMaterial', 'ngMessages']);


////Router
    app.config(['$routeProvider',function ($routeProvider) {
        $routeProvider
            .when("/", {
                controller: "form_test_controller",
                controllerAs: "form_test_controller",
                templateUrl: "frontend/modules/form_test/view/form_test_controller.view.html"
            })
            .when("/directive", {
                controller: "directive_test_controller",
                controllerAs: "directive_test_controller",
                templateUrl: "frontend/modules/directive_test/view/directive_test.view.html"
            })
            .when("/apiTest", {
                controller: "apiTest_controller",
                controllerAs: "apiTest_controller",
                templateUrl: "frontend/modules/apiTest/view/apiTest_controller.view.html"
            })
            .when("/transport", {
                controller: "transport_test_controller",
                controllerAs: "transport_test_controller",
                templateUrl: "frontend/modules/transport/view/transport_test_controller.view.html"
            })
            .when("/transport/:thing1/:thing2/:thing3", {
                templateUrl: "frontend/modules/transport/view/transport_test_controller.view.html",
                resolve: {
                    update: function ($route) {
                        console.log($route.current.params);
                       
                    }
                }
            })
            .when("/calculadora", {
                controller: "calculadora_controller",
                controllerAs: "calculadora_controller",
                templateUrl: "frontend/modules/calculadora/view/calc.view.html"
            })
    }]);
////Router 