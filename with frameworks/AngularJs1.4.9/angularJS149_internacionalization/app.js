var app = angular.module('myApp', ['pascalprecht.translate']);

app.run(['$rootScope', function($rootScope) {
    if(localStorage.getItem("lang")){
        console.log( localStorage.getItem("lang"));
        $rootScope.lang=localStorage.getItem("lang");
    }else{
        console.log("no idiom");
        localStorage.setItem("lang","en");
        $rootScope.lang="en";
    }
}])
app.controller('Ctrl', function ($scope, $rootScope, $translate) {
    $scope.changeLanguage = function (key) {
        console.log(key);
        localStorage.setItem("lang",key);
        $rootScope.lang = localStorage.getItem("lang");
        $translate.use($rootScope.lang);
    };
});
app.config(function ($translateProvider) {
    $translateProvider
            .useStaticFilesLoader({
                prefix: 'frontend/utils/lang/menu_',
                suffix: '.json'
            }) 
            // remove the warning from console log by putting the sanitize strategy
            .useSanitizeValueStrategy('sanitizeParameters')    
            .preferredLanguage(localStorage.getItem("lang"));   
});