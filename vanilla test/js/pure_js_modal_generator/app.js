const modal = (function(){
    let data;let prefix_id="modal-";
    let id;let div;let panel;let title;let close;
    let content;let footer;let mod;let bOk;let bCancel;
    function init(sufix_id, sended_data, ok, cancel) {
        create_modal(sufix_id);
        put_data(sended_data);
        if (ok && ok.length>1) {
            ok_button(ok);
        }
        if (cancel && cancel.length>1) {
            cancel_button(cancel);
        }
        if (footer.innerHTML=='') {
            delete_footer();
        }
        put_visible();
        
    }
    function create_modal(sufix_id) {
        id=prefix_id+sufix_id;
        if (document.getElementById(id)==null) { // solo hace falta crearla si no existe
            div=document.createElement("div");
            div.className="pure_modal"; // clase para estilizarla vÃ­a CSS
            div.id=id;
            // creamos el panel interior
            (panel=document.createElement("div")).setAttribute('class','panel');
            // creamos los componentes de la cabecera: tÃ­tulo y botÃ³n de cerrar
            (title=document.createElement("div")).setAttribute('class',"title");
            ((close=document.createElement("div")).setAttribute('class',"close"));
            close.innerHTML='Ã';
            // cerramos y vaciamos la modal al pulsar el botÃ³n X
            close.addEventListener('click',function(event) {
                event.preventDefault();
                var dTop=this.parentNode.parentNode;
                dTop.classList.remove("visible");
                dTop.querySelector(".panel .content").innerHTML='';
            });
            // creamos la caja donde se mostrarÃ¡ el contenido
            (content=document.createElement("div")).setAttribute('class','content');
            // tambiÃ©n aÃ±adimos un pie, para aÃ±adir los posibles botones
            (footer=document.createElement("div")).setAttribute('class',"footer");
            // y unimos los componentes
            panel.appendChild(title);panel.appendChild(close);
            panel.appendChild(content);panel.appendChild(footer);
            div.appendChild(panel);
            document.body.appendChild(div);
        }
        // guardamos cada componente en una variable
        mod=document.getElementById(id),
        panel=mod.querySelector(".panel"),
        title=mod.querySelector(".panel .title"),
        content=mod.querySelector(".panel .content"),
        footer=mod.querySelector(".panel .footer");
        if (footer==null) {
            // puede ocurrir que el footer no exista, asi que lo volvemos a crear
            mod.classList.remove("nofooter");
            (footer=document.createElement("div")).setAttribute('class',"footer");
            panel.appendChild(footer);
        }
    }
    function put_data(sended_data) {
        data=sended_data || {};
        // rellenamos los datos
        title.innerHTML=data.title || '';
        content.innerHTML=data.content || '';
        footer.innerHTML='';
        // comprobamos que el nÃºmero es vÃ¡lido antes de aÃ±adirlo
        if (!isNaN(data.width)) panel.style.maxWidth=data.width+'%';
        if (!isNaN(data.height)) panel.style.maxHeight=data.height+'%';
    }
    function ok_button(ok){
        // creamos el botÃ³n OK
        (bOk=document.createElement("button")).setAttribute('class',"action");
        bOk.innerHTML=ok[0];
        bOk.addEventListener('click',function(ev) {
            ev.preventDefault();
            mod.classList.remove("visible");
            ok[1]();
        });
        // aÃ±adimos el botÃ³n al footer
        footer.appendChild(bOk);
    }
    function cancel_button(cancel){
        (bCancel=document.createElement("button")).setAttribute('class',"action");
        bCancel.innerHTML=cancel[0];
        bCancel.addEventListener('click',function(ev) {
            ev.preventDefault();
            mod.classList.remove("visible");
            cancel[1]();
        });
        footer.appendChild(bCancel);
    }
    function delete_footer() {
        panel.removeChild(footer);
        mod.classList.add("nofooter");
    }
    function put_visible(){
        setTimeout(function(){
            mod.classList.add("visible");
        },50);
    }
    return{
        show:init
    }
})();