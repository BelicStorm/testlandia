/* const url = ''; En el caso de que necesites una url mas larga o compleja*/
window.onload = function()                                                                 /// Core de funcionamiento
{
    const delegate = (selector) => (test) => (e) => e.target.matches(selector) && test(e);/// Esta parte crea un evento que es delegado a 
    const inputDelegate = delegate('select[class=country_selector]');                     /// todos los select con la clase country selector.
    document.addEventListener('change', inputDelegate((input)=>{                          /// El evento se disparara cuando los selects cambien y 
                                                                                            /// de estos obtendremos el bloque de codigo que ha disparado el evento.                         
        let indicador_del_modulo=input.target.getAttribute("id").split('-')[0];           /// Con el bloque de codigo guardado en input, obtendremos los datos que necesitamos.
        let type_of_select=input.target.getAttribute("id").split('-')[1];
        let selected_index=input.target[input.target.selectedIndex];                      /// (Obtiene el option seleccionado)

        switch (type_of_select) {                                                         /// Tras obtener la info. necesaria sabremos que select se ha disparado y
            case "country_selector":                                                      /// que logica tendra que adaptar el core.
                let country_letter_code= selected_index.id                                
                get_provinces(country_letter_code,indicador_del_modulo);
                break;
            case "provinces_selector":
                let province_code = selected_index.value;                                  
                get_cities(province_code,indicador_del_modulo);
                break;
            case "citie_selector":
                let postal_code=selected_index.value;          
                get_postal_code(postal_code,indicador_del_modulo);
                break;
        }

    }));
} 
                                                                                           //Pais    //Modulo emisor ejem:(gt_clientes)
function get_provinces(country,indicador_del_modulo) {                                    /// Obtiene el json de las provincias y llama a la funcion que 
    try {                                                                                 /// las aÃ±ade a su respectivo select.
        get_json_fetch(country+"_province.json",function(response) {  
              /* var actual_JSON = JSON.parse(response);                                  /// Con XMLHttpRequest()
              expand_dropdown(actual_JSON,indicador_del_modulo+"-provinces_selector"); */ /// Con XMLHttpRequest()

              expand_dropdown(response,indicador_del_modulo+"-provinces_selector");       /// Con fetch()
           });
        
    } catch (error) {
        console.error('Error -', error);  
    }
}                 
                                                                                          // ejem:(04)    //Modulo emisor ejem:(gt_clientes)
function get_cities(province_code,indicador_del_modulo) {                                /// Obtiene el json de las ciudades y llama a la funcion que 
    try {                                                                               /// las aÃ±ade a su respectivo select.
        get_json_fetch("es_cities.json",function(response) {
                /* var actual_JSON = JSON.parse(response);                              /// Con XMLHttpRequest()
                let cities_of_selected_province = actual_JSON.filter(actual_JSON =>     /// Con XMLHttpRequest()
                    actual_JSON.id.startsWith(province_code)); */                       /// Con XMLHttpRequest()
                let cities_of_selected_province = response.filter(response =>           /// Con fetch()
                    response.id.startsWith(province_code));                             /// Con fetch()

                expand_dropdown(cities_of_selected_province,indicador_del_modulo+"-citie_selector");
            });
    } catch (error) {
        console.error('Error -', error);  
    }
}
                                                                                        //ejem:46890    //Modulo emisor ejem:(gt_clientes)
function get_postal_code(postalcode,indicador_del_modulo){                              /// AÃ±ade el codigo postal de la ciudad seleccionada al input correspondiente
    document.getElementById(indicador_del_modulo+'-postalcode_input').value=postalcode;
}


function expand_dropdown(data,indicador_del_modulo){                                    /// Funcion que aÃ±ade los datos obtenidos a los selects
    let option;

    document.getElementById(indicador_del_modulo).innerHTML='';                         /// Vaciamos el select y creamos un option vacio
    option = document.createElement('option');
    option.text = "----";
    document.getElementById(indicador_del_modulo).add(option);
 
    for (let i = 0; i < data.length; i++) {                                             /// AÃ±adimos el contenido del array al select
        option = document.createElement('option');
        option.text = data[i].nm;
        option.value = data[i].id;
        document.getElementById(indicador_del_modulo).add(option);

    }
}

function get_json_XMLHttpRequest(url, callback){                                        /// Obtenemos los datos del fichero json pasado en la variable url
    var xobj = new XMLHttpRequest();                                                     // Tenemos que usar un callback anonimo o NO RETORNARA NADA 
    xobj.overrideMimeType("application/json");                                          // simplemente obtendremos undefined
    xobj.open('GET', url, true); 
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
}   
function get_json_fetch(url,callback){                                                  /// Obtenemos los datos del fichero json pasado en la variable url
    fetch(url)                                                                          // Tenemos que usar un callback anonimo o NO RETORNARA NADA 
  .then(                                                                               // simplemente obtendremos undefined
    function(response) {  
      if (response.status !== 200) {  
        console.warn('Looks like there was a problem. Status Code: ' + 
          response.status);  
        return;  
      }
      response.json().then(function(data) {  
        callback(data);
      });  
    }  
  )  
  .catch(function(err) {  
    console.error('Fetch Error -', err);  
  });
}

  