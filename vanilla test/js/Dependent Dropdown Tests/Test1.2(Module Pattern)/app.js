const dropdown = (function(){
    const url = '../assets/country_data/';
    function init_all() {
        prepare();
    }
    function prepare(){
        let delegate = (selector) => (test) => (e) => e.target.matches(selector) && test(e);
        let inputDelegate = delegate('select[class=country_selector]');
        document.body.addEventListener('change', inputDelegate((input)=>{                                  
            let indicador_del_modulo=input.target.getAttribute("id").split('-')[0];          
            let type_of_select=input.target.getAttribute("id").split('-')[1];
            let selected_index=input.target[input.target.selectedIndex];                      
            switch (type_of_select) {                                                         
                case "country_selector":                                                      
                    country_letter_code= selected_index.id                                
                    get_provinces(country_letter_code,indicador_del_modulo);
                break;
                case "provinces_selector":
                    province_code = selected_index.value;                                  
                    get_cities(province_code,indicador_del_modulo);
                break;
                case "citie_selector":
                    postal_code=selected_index.value;          
                    get_postal_code(postal_code,indicador_del_modulo);
                break;
            }
        })); 
    }
    async function get_data(url,callback){
        try {                                                                                 
            await get_json_fetch(url,function(response) {  
                 callback (response);
            });
        } catch (error) {
            console.error('Error -', error);  
        }
    }
    async function get_provinces(country_letter_code,indicador_del_modulo) {  
        get_data(url+country_letter_code+"_province.json",function(data){
            expand_dropdown(data,indicador_del_modulo+"-provinces_selector");  
        });                                 
    }
    function get_cities(province_code,indicador_del_modulo) {
        get_data(url+"es_cities.json",function(data){
            expand_dropdown(data.filter(response =>response.id.startsWith(province_code)),indicador_del_modulo+"-citie_selector");
        });                            
    }
    function get_postal_code(postal_code,indicador_del_modulo){                              
        document.getElementById(indicador_del_modulo+'-postalcode_input').value=postal_code;
    }
    function expand_dropdown(data,indicador_del_modulo){                                    
        let option;
        document.getElementById(indicador_del_modulo).innerHTML='';                         
        option = document.createElement('option');
        option.text = "----";
        document.getElementById(indicador_del_modulo).add(option);
        for (let i = 0; i < data.length; i++) {                                             
            option = document.createElement('option');
            option.text = data[i].nm;
            option.value = data[i].id;
            document.getElementById(indicador_del_modulo).add(option);
        }
    }
    function get_json_XMLHttpRequest(url, callback){                                        
        let xobj = new XMLHttpRequest();                                                     
        xobj.overrideMimeType("application/json");                                          
        xobj.open('GET', url, true); 
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                callback(JSON.parse(xobj.responseText));
            }
        };
        xobj.send(null);  
    }   
    function get_json_fetch(url,callback){                                                  
        fetch(url)                                                                           
      .then(                                                                               
        function(response) {  
          if (response.status !== 200) {  
            console.warn('Looks like there was a problem. Status Code: ' + 
              response.status);  
            return;  
          }
          response.json().then(function(data) {  
            callback(data);
          });  
        }  
      )  
      .catch(function(err) {  
        console.error('Fetch Error -', err);  
      });
    }
    return{
        run:init_all
    }     
})();

window.onload = (function(){
    dropdown.run();
})();