const tableGenerator = (function(){ //creamos el objeto y declaramos la variables y metodos
    let tableHeader="";
    let table_body="";
    let table_footer="";
    let generator_id="";
    let class_generated="";
    let type_of_table="";
    let data_label="";
    
    function get_data(url,callback){ //creamos el metodo privado para obtener los datos de la tabla
        let xobj = new XMLHttpRequest();                                                     
        xobj.overrideMimeType("application/json");                                          
        xobj.open('GET', url, true); 
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                callback(JSON.parse(xobj.responseText));
            }
        };
        xobj.send(null); 
    }
    function table_generator(data){//creamos el metodo privado para generar la tabla
        let count = Object.keys(data.table).length;
        
        let row=0;
        let data_label_count=0;
        let tableContent = "<tbody><tr>";
        for(let i = 0; i < count; i++) {
            if(row==data.table[i].row){
                data_label_count++;
                tableContent+=`<td data-label='${data_label[data_label_count]}' id='${type_of_table}' class='relleno ${class_generated}'>${data.table[i].column}</td>`;
            }else{
                row++;
                data_label_count=0;
                tableContent+=`</tr><tr><td data-label='${data_label[data_label_count]}' id='${type_of_table}' class='relleno ${class_generated}'>${data.table[i].column}</td>`
            }
        }
        tableContent += "</tbody></tr>";
        return tableContent;
        
    }

    return{
        send_data_to_generate_table(options) { //creamos el metodo publico.
            get_data(options.url,function(data){

                tableHeader=options.header;
                table_footer=options.footer;
                generator_id=options.from_id;
                class_generated=options.to_class;
                type_of_table=options.table_type;
                data_label=options.labels;
                table_body=table_generator(data);
                
                // Get div and output the HTML.
                document.getElementById(generator_id).innerHTML = tableHeader + table_body + table_footer;
            }) 
        }
    }
    
})();