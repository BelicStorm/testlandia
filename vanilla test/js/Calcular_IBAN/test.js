var IBAN = (function()
{
    var paisOmision = "es";
    // Ejemplo: calcularIBAN("1234-5678-06-1234567890", "es") --> "ES6812345678061234567890"
    function calcularIBAN(ccc, pais) {
      ccc = limpiar(ccc);
      pais = (pais==undefined? paisOmision: pais).toUpperCase();
      var cifras = ccc + valorCifras(pais) + "00";
      var resto = modulo(cifras, 97);
      return pais + cerosIzquierda(98-resto, 2) + ccc;
    }
    // Ejemplo1: validarIBAN("ES00 1234 5678 0612 3456 7890") --> false
    // Ejemplo2: validarIBAN("ES68 1234 5678 0612 3456 7890") --> true
    function validarIBAN(iban) {
      iban = limpiar(iban);
      var pais = iban.substr(0, 2);
      var dc = iban.substr(2, 2);
      var cifras = iban.substr(4, 20) + valorCifras(pais) + dc;
      resto = modulo(cifras, 97);
      return resto == 1;
    }
    // Ejemplo1: validarCCC("1234-5678-00-1234567890") --> false
    // Ejemplo2: validarCCC("1234-5678-06-1234567890") --> true
    function validarCCC(ccc) {
      ccc = limpiar(ccc);
      var items = formatearCCC(ccc, " ").split(" ");
      var dc = modulo11(items[0] + items[1]) + "" + modulo11(items[3]);
      return dc == items[2];
    }
    // Ejemplo: calcularCCC("1234-5678-??-1234567890") --> "12345678061234567890"
    function calcularCCC(ccc) {
      ccc = limpiar(ccc);
      return ccc.substr(0,8) + calcularDC(ccc) + ccc.substr(10,10);
    }
    // Ejemplo: calcularDC("1234-5678-??-1234567890") --> "06"
    function calcularDC(ccc) {
      ccc = limpiar(ccc);
      var items = formatearCCC(ccc, " ").split(" ");
      return modulo11(items[0] + items[1]) + "" + modulo11(items[3]);
    }
    // Ejemplo: formatearCCC("12345678061234567890") --> "1234-5678-06-1234567890"
    function formatearCCC(ccc, separador) {
      ccc = limpiar(ccc);
      if (separador==undefined) separador = "-";
      return ccc.substr(0,4) + separador + ccc.substr(4,4) + separador +
              ccc.substr(8,2) + separador + ccc.substr(10,10);
    }
    // Ejemplo: formatearIBAN("ES6812345678061234567890") --> "ES68 1234 5678 0612 3456 7890"
    function formatearIBAN(iban, separador) {
      iban = limpiar(iban);
      if (separador==undefined) separador = " ";
      var items = [];
      for (var i=0; i<6; i++) { items.push(iban.substr(i*4, 4)); }
      return items.join(separador);
    }
    function esCCC(cifras) {
      ////return /^(\d{20})$/i.test(cifras);
      return cifras.length == 20;
    }
    function esIBAN(cifras) {
      ////return /^(\d{24})$/i.test(cifras);
      return cifras.length == 24;
    }
    // Ejemplo1: sonDigitos("1234") --> true
    // Ejemplo2: sonDigitos("12e4") --> false
    function sonDigitos(cifras) {
      var er = new RegExp("^(\\d{"+cifras.length+"})$", "i");
      return er.test(cifras);
    }
    // Ejemplo: limpiar("IBAN1234 5678-90") --> "1234567890"
    function limpiar(numero) {
      return numero
        .replace(/IBAN/g, "")
        .replace(/ /g, "")
        .replace(/-/g, "");
    }
    // Ejemplo: modulo("12345678061234567890142800", 97) --> 30
    function modulo(cifras, divisor) {
      var CUENTA = 10;
      var largo = cifras.length;
      var resto = 0;
      for (var i=0; i<largo; i+=CUENTA) {
        var dividendo = resto + "" + cifras.substr(i, CUENTA);
        resto = dividendo % divisor;
      }
      return resto;
    }
    // Ejemplo1: modulo11("12345678") --> "0"
    // Ejemplo2: modulo11("1234567890") --> "6"
    function modulo11(cifras) {
      var modulos = [1, 2, 4, 8, 5, 10, 9, 7, 3, 6]; //2^index % 11
      var suma = 0;
      var cifras = cerosIzquierda(cifras, 10);
      for (var i=0; i<cifras.length; i++) {
        suma += parseInt(cifras[i]) * modulos[i];
      }
      var control = suma % 11;
      return control < 2? control: 11 - control;
    }
    // Ejemplo: cerosIzquierda("7", 3) --> "007"
    function cerosIzquierda(cifras, largo) {
      cifras += '';
      while(cifras.length < largo) { cifras = '0'+cifras; }
      return cifras;
    }
    // Ejemplo: valorCifras("es") --> "1428"
    function valorCifras(cifras) {
      var LETRAS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // A=10, B=11, ... Z=35
      var items = [];
      for (var i=0; i<cifras.length; i++) {
        var posicion = LETRAS.indexOf(cifras[i]);
        items.push(posicion < 0? "-": posicion);
      }
      return items.join("");
    }
    function convertir(numero, pais) {
        numero = limpiar(numero);
        var iban = numero.substr(numero.length-24, 24);
        var ccc = numero.substr(numero.length-20, 20);
        if (!esIBAN(numero) && !esCCC(numero)) return "Error: No es IBAN ni CCC";
        else if (esIBAN(numero) && !validarIBAN(iban)) return "Error: IBAN incorrecto";
        else if (!validarCCC(ccc)) return "Error: CCC incorrecto";
        else if (esIBAN(numero)) return formatearIBAN(iban);
        else return formatearIBAN(calcularIBAN(ccc, pais));
    }
    // Ejemplo: IBAN.calcular("1234-5678-??-1234567890") --> "ES6812345678061234567890" (68 y 06)
    function calcular(numero, pais) {
        numero = limpiar(numero);
        if (esCCC(numero)) {
            var dc = numero.substr(8,2);
            if (!sonDigitos(dc)) numero = calcularCCC(numero);
            return calcularIBAN(numero, pais);
        }
        else return numero;
    }
    // Ejemplo1: IBAN.validar("ES68 1234 5678 0612 3456 7890") --> true (68)
    // Ejemplo2: IBAN.validar("1234-5678-06-1234567890") --> true (06)
    function validar(numero) {
        numero = limpiar(numero);
        if (esIBAN(numero)) return validarIBAN(numero);
        else if (esCCC(numero)) return validarCCC(numero);
        else return false;
    }
    // Ejemplo: IBAN.formatear("12345678061234567890") --> "1234-5678-06-1234567890"
    // Ejemplo: IBAN.formatear("ES6812345678061234567890") --> "ES68 1234 5678 0612 3456 7890"
    function formatear(numero, separador) {
        numero = limpiar(numero);
        if (esIBAN(numero)) return formatearIBAN(numero, separador);
        else if (esCCC(numero)) return formatearCCC(numero, separador);
        else return "";
    }
    //--------------------------------------------
    return {
      convertir: convertir,
      calcular: calcular,
      validar: validar,
      formatear: formatear,
    };

})();