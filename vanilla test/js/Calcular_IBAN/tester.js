document.addEventListener("DOMContentLoaded", function () {
    Tests.init();
    Form.run();
  });

  var Tests = {

    init: function() {
      var button = document.getElementById("btn_tests");
      button.addEventListener('click', Tests.run, false);
    },
  
    run: function() {
      var ol = document.createElement('OL');
      var test = function(func, num, ref) { ol.appendChild(Tests.test(func, num, ref)); }
      var gap = function() { ol.appendChild(document.createElement('BR')); }
      var container = document.getElementById("tests");
      //
      test(IBAN.convertir, "1234567890", "Error: No es IBAN ni CCC");
      test(IBAN.convertir, "ES0012345678061234567890", "Error: IBAN incorrecto");
      test(IBAN.convertir, "12341234001234567890", "Error: CCC incorrecto");
      test(IBAN.convertir, "ES5212345678001234567890", "Error: CCC incorrecto");
      test(IBAN.convertir, "ES6812345678061234567890", "ES68 1234 5678 0612 3456 7890");
      test(IBAN.convertir, "1234-5678-06-1234567890", "ES68 1234 5678 0612 3456 7890");
      gap();
      test(IBAN.calcular, "12345678061234567890", "ES6812345678061234567890");
      test(IBAN.calcular, "3141-5926-XX-5358979323", "ES4031415926955358979323");
      test(IBAN.calcular, "16180339188874989484", "ES6616180339188874989484");
      test(IBAN.calcular, "2718-2818-16-2845904523", "ES5627182818162845904523");
      test(IBAN.calcular, "14142135??6237309504", "ES6514142135996237309504");
      test(IBAN.calcular, "23571113141719232931", "ES6523571113141719232931");
      gap();
      test(IBAN.validar, "ES6523571113141719232931", true);
      test(IBAN.validar, "ES7723571113141719232931", false);
      test(IBAN.validar, "27182818162845904523", true);
      test(IBAN.validar, "27182818992845904523", false);
      gap();
      test(IBAN.formatear, "ES6616180339188874989484", "ES66 1618 0339 1888 7498 9484");
      test(IBAN.formatear, "31415926955358979323", "3141-5926-95-5358979323");
      //
      container.innerHTML = '';
      container.appendChild(ol);
    },
  
    test: function (func, num, ref) {
      var result = func(num);
      var ok = result == ref? "ok": "error";
      var msg = "<span class=\""+ok+"\">'"+result+"'</span>";
      if (result != ref) msg += " != <span>'"+ref+"'</span>";
      var li = document.createElement('LI');
      li.innerHTML = "<code>IBAN."+func.name+"('"+num+"') = "+msg+"</code> &rarr; "+ok;
      return li;
    },
  
  };

  //================================================

var Form = (function() {

    var form, btn, entidad, oficina, dc, cuenta, iban;
  
    function start() {
      init_all();
      observer();
    }
  
    function init_all() {
      form    = document.getElementById("form1");
      btn     = document.getElementById("btn_intro");
      entidad = document.getElementById('entidad');
      oficina = document.getElementById('oficina');
      dc      = document.getElementById('dc');
      cuenta  = document.getElementById('cuenta');
      iban    = document.getElementById('iban');

      form.addEventListener('submit', submit, false);
      btn.addEventListener('click', intro, false);
    }
    
    function submit(event) {
      iban.value = IBAN.calcular("" + entidad.value + oficina.value +
        (dc.value == ""? "XX": dc.value) + cuenta.value);
      iban.value = IBAN.formatear(iban.value);
      event.preventDefault();
      return false;
    }
  
    function intro(event) {
      var ccc = prompt("CCC");
      if (!ccc) return;
      var items = IBAN.formatear(ccc, " ").split(" ");
      if (items.length != 4) return alert("CCC incorrecto");
      entidad.value = items[0];
      oficina.value = items[1];
      dc.value      = items[2];
      cuenta.value  = items[3];
    }
  
    return {
      run: start,
    };
  
  })();
  