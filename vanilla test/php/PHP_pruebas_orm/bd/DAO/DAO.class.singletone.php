<?php
    class DAO {
        public $content="";
        static $_instance;

            private function __construct() {

            }
            public static function getInstance() {
                if(!(self::$_instance instanceof self)){
                    self::$_instance = new self();
                }
                return self::$_instance;
            }

            public function select($rows,$tabla){
                $this->content= "SELECT $rows from $tabla";
            }
            public function join_argument($table){
                $this->content= $this->content . " inner join " . $table;
            }
            public function on_argument($argument){
                 $this->content= $this->content . " on " . $argument;
            }
            public function where_argument($argument){
                $this->content= $this->content . " where " . $argument;
            }
            public function and_argument($argument){
                 $this->content= $this->content . " and " . $argument;
            }
            public function or_argument($argument){
                 $this->content= $this->content . " or " . $argument;
            }
            public function order_argument($by,$form){
                 $this->content= $this->content . " order by " . $by . " " . $form;
            }
            public function limit_argument($number){
                 $this->content= $this->content . " limit " . $number;
            }
            public function sub_select($rows,$tabla){
                $this->content= $this->content . " SELECT $rows from $tabla ";
            }

            //ejecutar sentencias
            public static function get($db){
                return $db->listar($db->ejecutar(self::$_instance->content));
            }
            public static function post($db, $arrArgument){
                
            }
            public static function put($db, $arrArgument){
                
            }
            public static function delete($db, $arrArgument){
                
            }
            //ejecutar sentencias
    }