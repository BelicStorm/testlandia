<?php
include_once('test.php');
echo "Ejemplos de uso: </br></br>";
     echo $result1=IBAN::convertir("1234-5678-06-1234567890")."</br>";      /* --> "ES68 1234 5678 0612 3456 7890" */
     echo $result2=IBAN::calcular("1234-5678-??-1234567890")."</br>";       /*  --> "ES6812345678061234567890" (68 y 06) */
     echo $result3=IBAN::validar("ES68 1234 5678 0612 3456 7890")."</br>";  /*  --> true (68) */
     echo $result4=IBAN::validar("1234-5678-06-1234567890")."</br>";        /*  --> true (06) */
     echo $result5=IBAN::formatear("12345678061234567890")."</br>";         /* --> "1234-5678-06-1234567890" (guiones) */
     echo $result6=IBAN::formatear("ES6812345678061234567890")."</br>";     /* --> "ES68 1234 5678 0612 3456 7890"  */


    
