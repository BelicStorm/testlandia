<?php

class IBAN {

    private static $paisOmision = "es";
///Privated
    private static function esCCC($cifras) {
        return strlen($cifras) == 20;
    }
    private static function esIBAN($cifras) {
        return strlen($cifras) == 24;
    }
    // Ejemplo1: sonDigitos("1234") --> true
    // Ejemplo2: sonDigitos("12e4") --> false
    private static function sonDigitos($cifras) {
        $er = "^\\d{".strlen($cifras)."}$";
        return @preg_match_all($er, $cifras) > 0;
    }
    // Ejemplo: limpiar("IBAN1234 5678-90") --> "1234567890"
    private static function limpiar($numero) {
        return
        str_replace("IBAN", "",
        str_replace(" ", "",
        str_replace("-", "", $numero)));
    }
    // Ejemplo: modulo("12345678061234567890142800", 97) --> 30
    private static function modulo($cifras, $divisor) {
        $CUENTA = 7;
        $len = strlen($cifras);
        $resto = 0;
        for ($i=0; $i<$len; $i+=$CUENTA) {
        $dividendo = $resto . substr($cifras, $i, $CUENTA);
        $resto = intval($dividendo) % $divisor;
        }
        return $resto;
    }
    // Ejemplo1: modulo11("12345678") --> "0"
    // Ejemplo2: modulo11("1234567890") --> "6"
    private function modulo11($cifras) {
        $MODULOS = array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6); //2^index % 11
        $suma = 0;
        $cifras = self::cerosIzquierda($cifras, 10);
        for ($i=0; $i<strlen($cifras); $i++) {
        $suma += intval($cifras[$i]) * $MODULOS[$i];
        }
        $control = $suma % 11;
        return $control < 2? $control: 11 - $control;
    }
    // Ejemplo: cerosIzquierda("7", 3) --> "007"
    private static function cerosIzquierda($cifras, $largo) {
        return str_pad($cifras, $largo, "0", STR_PAD_LEFT);
    }
    // Ejemplo: valorCifras("es") --> "1428"
    private static function valorCifras($cifras) {
        $LETRAS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // A=10, B=11, ... Z=35
        $items = array();
        for ($i=0; $i<strlen($cifras); $i++) {
        $posicion = strpos($LETRAS, $cifras[$i]);
        $items[] = $posicion < 0? "-": $posicion;
        }
        return implode("", $items);
    }
///Privated
    
///Protected
    // Ejemplo: calcularIBAN("1234-5678-06-1234567890", "es") --> "ES6812345678061234567890"
    protected static function calcularIBAN($ccc, $pais="es") {
      $ccc = self::limpiar($ccc);
      $pais = strtoupper($pais==null? $paisOmision: $pais);
      $cifras = $ccc . self::valorCifras($pais) . "00";
      $resto = self::modulo($cifras, 97);
      return $pais . self::cerosIzquierda(98-$resto, 2) . $ccc;
    }
    // Ejemplo1: validarIBAN("ES00 1234 5678 0612 3456 7890") --> false
    // Ejemplo2: validarIBAN("ES68 1234 5678 0612 3456 7890") --> true
    protected static function validarIBAN($iban) {
      $iban = self::limpiar($iban);
      $pais = substr($iban, 0, 2);
      $dc = substr($iban, 2, 2);
      $cifras = substr($iban, 4, 20) . self::valorCifras($pais) . $dc;
      $resto = self::modulo($cifras, 97);
      return $resto == 1;
    }
    // Ejemplo1: validarCCC("1234-5678-00-1234567890") --> false
    // Ejemplo2: validarCCC("1234-5678-06-1234567890") --> true
    protected static function validarCCC($ccc) {
      $ccc = self::limpiar($ccc);
      $items = explode(" ", self::formatearCCC($ccc, " "));
      $dc = self::modulo11($items[0] . $items[1]) . self::modulo11($items[3]);
      return $dc == $items[2];
    }
    // Ejemplo: calcularCCC("1234-5678-??-1234567890") --> "12345678061234567890"
    protected static function calcularCCC($ccc) {
      $ccc = self::limpiar($ccc);
      return substr($ccc,0,8) . self::calcularDC($ccc) . substr($ccc,10,10);
    }
    // Ejemplo: calcularDC("1234-5678-??-1234567890") --> "06"
    protected static function calcularDC($ccc) {
      $ccc = self::limpiar($ccc);
      $items = explode(" ", self::formatearCCC($ccc, " "));
      return self::modulo11($items[0] . $items[1]) . self::modulo11($items[3]);
    }
    // Ejemplo: formatearCCC("12345678061234567890") --> "1234-5678-06-1234567890"
    protected static function formatearCCC($ccc, $separador=null) {
      if ($separador == null) $separador = "-";
      $ccc = self::limpiar($ccc);
      return substr($ccc,0,4) . $separador . substr($ccc,4,4) . $separador .
              substr($ccc,8,2) . $separador . substr($ccc,10,10);
    }
    // Ejemplo: formatearIBAN("ES6812345678061234567890") --> "ES68 1234 5678 0612 3456 7890"
    protected static function formatearIBAN($iban, $separador=null) {
      if ($separador == null) $separador = " ";
      $iban = self::limpiar($iban);
      $items = array();
      for ($i=0; $i<6; $i++) { $items[] = substr($iban, $i*4, 4); }
      return implode($separador, $items);
    }
///Protected

///Public Functions
    public static function convertir($numero, $pais="es") {
        $numero = self::limpiar($numero);
        $iban = substr($numero, -24);
        $ccc = substr($numero, -20);
        if (!self::esIBAN($numero) && !self::esCCC($numero)) return "Error: No es IBAN ni CCC";
        else if (self::esIBAN($numero) && !self::validarIBAN($iban)) return "Error: IBAN incorrecto";
        else if (!self::validarCCC($ccc)) return "Error: CCC incorrecto";
        else if (self::esIBAN($numero)) return self::formatearIBAN($iban);
        else return self::formatearIBAN(self::calcularIBAN($ccc, $pais));
      }
  
    // Ejemplo: IBAN::calcular("1234-5678-??-1234567890") --> "ES6812345678061234567890" (68 y 06)
    public static function calcular($numero, $pais="es") {
        $numero = self::limpiar($numero);
        if (self::esCCC($numero)) {
            $dc = substr($numero, 8, 2);
            if (!self::sonDigitos($dc)) $numero = self::calcularCCC($numero);
            return self::calcularIBAN($numero, $pais);
        }
        else return $numero;
    }
    // Ejemplo1: IBAN::validar("ES68 1234 5678 0612 3456 7890") --> true (68)
    // Ejemplo2: IBAN::validar("1234-5678-06-1234567890") --> true (06)
    public function validar($numero) {
        $numero = self::limpiar($numero);
        if (self::esIBAN($numero)) return self::validarIBAN($numero);
        else if (self::esCCC($numero)) return self::validarCCC($numero);
        else return false;
    }
    // Ejemplo: IBAN::formatear("12345678061234567890") --> "1234-5678-06-1234567890"
    // Ejemplo: IBAN::formatear("ES6812345678061234567890") --> "ES68 1234 5678 0612 3456 7890"
    public static function formatear($numero, $separador=null) {
        $numero = self::limpiar($numero);
        if (self::esIBAN($numero)) return self::formatearIBAN($numero, $separador);
        else if (self::esCCC($numero)) return self::formatearCCC($numero, $separador);
        else return "";
    }
///Public Functions
}

?>