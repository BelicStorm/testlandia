<?php
    class DAO {
        public $content="";
        static $_instance;

            private function __construct() {

            }
            public static function getInstance() {
                if(!(self::$_instance instanceof self)){
                    self::$_instance = new self();
                }
                return self::$_instance;
            }
    //Pure Statements
            public function select($rows,$tabla){
                $this->content= "SELECT $rows from $tabla";
            }
            public function insert($tabla){
                $this->content= "INSERT INTO $tabla";
            }
            //insert into options
                public function close_insert_into_statement(){
                        $this->content=$this->content . ")";
                }
               //columns
                    public function first_colum_insert_into($rows){
                        $this->content=$this->content . "($rows";
                    }
                    public function more_colums_insert_into($rows){
                        $this->content=$this->content . ",$rows";
                    }
               //columns
               //values
                    public function start_value_insert_into($values){
                        $this->content=$this->content . " VALUES ('$values'";
                    }
                    public function more_values_insert_into($values){
                        $this->content=$this->content . ",'$values'";
                    }
               //values
            //insert into options
             public function update($table){
                $this->content="UPDATE $table SET ";
            }
            //update options
                public function update_set($column,$argument){
                    $this->content=$this->content ." $column = '$argument'";
                }
                public function more_update_set($column,$argument){
                    $this->content=$this->content ." ,$column = '$argument'";
                }
            //update options
    //Pure Statements
    //Optional statement options
            public function where_argument($argument){
                $this->content= $this->content . " where " . $argument;
            }
            public function and_argument($argument){
                 $this->content= $this->content . " and " . $argument;
            }
            public function or_argument($argument){
                 $this->content= $this->content . " or " . $argument;
            }
    //Optional statement options
            //ejecutar sentencias
            public static function get($db){
                return $db->listar($db->ejecutar(self::$_instance->content));
            }
            public static function put($db){
                return $db->ejecutar(self::$_instance->content);
            }
            //ejecutar sentencias
    }