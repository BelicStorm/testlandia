var RPS = ( function(){
    let score = document.getElementById('score');
    let result = document.getElementById('result');
    let restart = document.getElementById('restart');
    let modal = document.querySelector('.modal');
    let scoreboard = {
        player: 0,
        computer: 0
    };
    // Get player winrate
    function Winrate(playerChoice){
      let win;
      switch (playerChoice) {
        case 'piedra':
          win = ['fuego','tijera','serpiente','humano','arbol','lobo','esponja'];
          break;
        case 'papel':
          win=['aire','agua','dragon','demonio','luz','pistola','piedra'];
          break;
        case 'tijera':
          win=['serpiente','humano','arbol','lobo','esponja','papel','aire'];
          break;
        case 'pistola':
          win=['piedra','fuego','tijera','serpiente','humano','arbol','lobo'];
          break;
        case 'luz':
          win=['pistola','piedra','fuego','tijera','serpiente','humano','arbol'];
          break;
        case 'dragon':
          win=['demonio','luz','pistola','piedra','fuego','tijera','serpiente'];
          break;
        case 'agua':
          win=['dragon','demonio','luz','pistola','piedra','fuego','tijera'];
          break;
        case 'aire':
          win=['agua','dragon','demonio','luz','pistola','piedra','fuego'];
          break;
        case 'esponja':
          win=['papel','aire','agua','dragon','demonio','luz','pistola'];
          break;
        case 'lobo':
          win=['esponja','papel','aire','agua','dragon','demonio','luz'];
        break;
        case 'arbol':
          win=['lobo','esponja','papel','aire','agua','dragon','demonio'];
          break;
        case 'humano':
          win=['arbol','lobo','esponja','papel','aire','agua','dragon'];
        break;
        case 'serpiente':
          win=['humano','arbol','lobo','esponja','papel','aire','agua'];
          break;
        case 'fuego':
          win=['tijera','serpiente','humano','arbol','lobo','esponja','papel'];
          break;
        case 'diablo':
          win=['luz','pistola','piedra','fuego','tijera','serpiente','humano'];
          break;
      }
      return win;
    }
    return{
        getWinner(playerChoice, computerChoice) {
          if (playerChoice === computerChoice) {
          return 'draw';
          } else{
            if (Winrate(playerChoice).includes(computerChoice)) {
              return 'player'
            }else{
              return 'player2'
            }
          }
        },
        showWinner(winner, computerChoice, playerChoice) {
          let vs = `<img src="images/${playerChoice}.png"><img class="vs" src="images/vs.png"><img src="images/${computerChoice}.png">
                    <p><strong>${playerChoice.charAt(0).toUpperCase() + playerChoice.slice(1)} VS ${computerChoice.charAt(0).toUpperCase() + computerChoice.slice(1)}</strong></p>`;
          
          
          if (winner === 'player') {
            // Inc player score
            scoreboard.player++;
            // Show modal result
            result.innerHTML = `<h1 class="text-win">Player 1 Wins</h1>${vs}`;
          } else if (winner === 'player2') {
            scoreboard.computer++;// Inc computer score
            result.innerHTML = `<h1 class="text-lose">Player 2 Wins </h1>${vs} `;  // Show modal result
          } else {
            result.innerHTML = `<h1>It's A Draw</h1>${vs}`;
          }
          score.innerHTML = `<p>Player1: ${scoreboard.player}</p><p>Player2: ${scoreboard.computer}</p>`; // Show score
          modal.style.display = 'block';
        },
        show_rules(){
            result.innerHTML=`<img id="result" src="images/rules.png">`;
            modal.style.display = 'block';
        },
        gamemode(e){
          sessionStorage.setItem('gamenode',e.target.id);
          RPS.restartGame();
        },
        restartGame() {
            scoreboard.player = 0;
            scoreboard.computer = 0;
            if (sessionStorage.getItem('gamenode')=="Two_players"){
              score.innerHTML = `<p>Player1: 0</p><p>Player2: 0</p>`;
            } else {
              score.innerHTML = `<p>Player1: 0</p><p>Computer: 0</p>`;
            }
        },
        clearModal(e) {
            if (e.target == modal) {
              modal.style.display = 'none';
            }
        },
        play(e){
          if (sessionStorage.getItem('gamenode')) {
              console.log(sessionStorage.getItem('gamenode'));
              switch (sessionStorage.getItem('gamenode')) {
                case 'Two_players':
                  RPS.TwoPlayers.play(e);
                  break;
              
                default:
                    RPS.vsComputer.play(e);
                  break;
              }
          }else{
            result.innerHTML=`Please First Select a Gamemode`;
            modal.style.display = 'block';
          }
        }
    }
   
  }());