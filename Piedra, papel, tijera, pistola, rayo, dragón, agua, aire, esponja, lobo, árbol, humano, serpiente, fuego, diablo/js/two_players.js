RPS.TwoPlayers = (function(){

return{
    play(e) {
        /* console.log(sessionStorage.getItem('player1')); */
        restart.style.display = 'inline-block';
        let playerChoice = e.target.id;

        if(sessionStorage.getItem('player1_did_a_movement')==null){
            console.log('Player1');
            sessionStorage.setItem('player1_did_a_movement',true);
            sessionStorage.setItem('player1_choice',playerChoice);
        }else {
            console.log('Player2');
            let player1_choice = sessionStorage.getItem('player1_choice');
            let player2_choice = playerChoice;

            const winner = RPS.getWinner(player1_choice, player2_choice);
            RPS.showWinner(winner, player2_choice, player1_choice);

            sessionStorage.removeItem('player1_did_a_movement');
            sessionStorage.removeItem('player1_choice');
        }
        
        
      }
}

})();