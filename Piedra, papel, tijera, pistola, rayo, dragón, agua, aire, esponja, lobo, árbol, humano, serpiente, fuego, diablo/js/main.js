// Event listeners
window.onload=function () {
    sessionStorage.clear();
    //Gamemode
    let gamemode;
    document.getElementById('Two_players').addEventListener('click',RPS.gamemode);
    document.getElementById('vsComputer').addEventListener('click',RPS.gamemode);


    //Gameplay
    const choices = document.querySelectorAll('.choice');
    choices.forEach(choice => choice.addEventListener('click', RPS.play));


    //Options
    document.getElementById('restart').addEventListener('click', RPS.restartGame);
    document.getElementById('show_rules').addEventListener('click', RPS.show_rules);

    window.addEventListener('click', RPS.clearModal);
};

