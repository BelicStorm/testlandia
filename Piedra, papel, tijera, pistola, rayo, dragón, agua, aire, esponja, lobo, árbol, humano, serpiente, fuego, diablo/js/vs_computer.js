RPS.vsComputer = (function(){
    let options=['piedra', 'papel', 'tijera', 'pistola', 'luz', 'dragon', 'agua', 'aire', 'esponja', 'lobo', 'arbol', 'humano', 'serpiente', 'fuego', 'diablo'];
    return{
        play(e) {
            restart.style.display = 'inline-block';
            let playerChoice = e.target.id;
            if(sessionStorage.getItem('player1_did_a_movement')){
                sessionStorage.removeItem('player1_did_a_movement');
                sessionStorage.removeItem('player1_choice'); 
            }
            // Get computers choice
            let computerChoice = options[Math.floor(Math.random() * options.length)];
            let player1_choice = playerChoice;

            const winner = RPS.getWinner(player1_choice, computerChoice);
            RPS.showWinner(winner, computerChoice, player1_choice);
          }
    }
    
    })();